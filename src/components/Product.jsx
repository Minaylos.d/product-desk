export const Product = ({ product }) => {
  return (
    <>
      {product.instock && (
        <div className="product">
          <div className="column__info">
            <div className="grow">
              <div className="product__name">
                {product.name.length > 22 ? (
                  <div className="h4 animate">
                    <div>{product.name}</div>
                    <div>{product.name}</div>
                  </div>
                ) : (
                  <div>{product.name}</div>
                )}
              </div>
              <div className="product__info">
                Алк. {product.alcohol}% Пл. {product.density}% {product.city}
              </div>
            </div>
          </div>
          <div className="column__price">
            {product.price}
            {product.priceByCard !== 0 && !product.discount.length && (
              <div className="stroke"></div>
            )}
          </div>
          {product.priceByCard !== 0 && !product.discount.length && (
            <div className="column__promo">{product.priceByCard}</div>
          )}
          {product.discount.length > 0 && (
            <div
              className={`${
                product.discount[0].length > 5
                  ? "promo__minimised"
                  : "column__promo"
              }`}
            >
              {product.discount[0]}
            </div>
          )}
          {product.priceByCard == 0 && !product.discount.length && (
            <div className="product__img__wrapper">
              <img
                className="product__img"
                src={product.additionally[0].img.src}
              />
            </div>
          )}
        </div>
      )}
      {!product.instock && (
        <div className="product">
          <div className="column__info">
            <div className="grow">
              <div className="product__name txt__outofstock">
                {product.name.length > 22 ? (
                  <div className="h4 animate">
                    <div>{product.name}</div>
                    <div>{product.name}</div>
                  </div>
                ) : (
                  <div>{product.name}</div>
                )}
              </div>
              <div className="product__info txt__outofstock">
                Алк. {product.alcohol}% Пл. {product.density}% {product.city}
              </div>
            </div>
          </div>
          <div className="column__price__outofstock txt__outofstock">
            В ПУТИ
          </div>
          <div className="product__img__wrapper">
            <img
              className="product__img img__outofstock"
              src={product.additionally[0].img.src}
            />
          </div>
        </div>
      )}
    </>
  );
};
