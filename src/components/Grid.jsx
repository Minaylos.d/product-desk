import { useEffect, useState } from "react";
import React from "react";
import { Product } from "./Product";

export const Grid = ({ divider, productData }) => {
  const categories = productData[0].groups;

  const [scrollPosition, setScrollPosition] = useState(0);

  useEffect(() => {
    const element = document.querySelector(".products__grid");
    const realWidth = element.scrollWidth;
    const scrollStep = window.innerWidth / divider;

    const scrollInterval = setInterval(() => {
      setScrollPosition((prevPosition) => {
        const newPosition = prevPosition + scrollStep;
        if (newPosition >= realWidth) {
          element.scrollTo(0, 0);
          return 0;
        } else {
          console.log(newPosition);
          element.scrollTo(newPosition, 0);
          return newPosition;
        }
      });
    }, 30000);

    return () => clearInterval(scrollInterval);
  }, []);

  function prepareData(categories) {
    const flatArray = [];

    for (let i = 0; i < categories.length; i++) {
      flatArray.push(categories[i].group);
      for (let j = 0; j < categories[i].products.length; j++) {
        flatArray.push(categories[i].products[j]);
      }
    }

    const columns = [];
    let currentColumn = [];
    let currentRowCount = 0;

    for (let i = 0; i < flatArray.length; i++) {
      const item = flatArray[i];

      if (currentRowCount === 10 && typeof flatArray[i] === "string") {
        columns.push(currentColumn);
        currentColumn = [];
        currentRowCount = 0;
      }

      if (currentRowCount >= 11) {
        columns.push(currentColumn);
        currentColumn = [];
        currentRowCount = 0;
      }

      if (typeof item === "string") {
        currentColumn.push({ type: "group", name: item });
        currentRowCount++;
      } else {
        currentColumn.push({ type: "product", name: item });
        currentRowCount++;
      }
    }

    columns.push(currentColumn);

    return columns;
  }

  const columns = prepareData(categories);

  return (
    <>
      <div className="products__grid">
        {columns.map((group, index) => (
          <div className="column__wrapper" key={index}>
            {group.map((item, index) =>
              item.type === "group" ? (
                <div className="info__wrapper" key={index}>
                  <div className="column__name">{item.name}</div>
                  <div className="column__name">Цена</div>
                  <div className="column__name">Акция</div>
                </div>
              ) : (
                <Product product={item.name} key={index} />
              )
            )}
          </div>
        ))}
        {productData[0].additionally[0].img.src === "" ? (
          ""
        ) : (
          <div className="poster__wrapper">
            <img
              className="poster"
              src={productData[0].additionally[0].img.src}
            ></img>
          </div>
        )}
      </div>
    </>
  );
};
