import { Header } from "@/components/Header";
import { Footer } from "@/components/Footer";
import { Grid } from "@/components/Grid";
import { useEffect, useState } from "react";
import Script from "next/script";
import "./globals.css";
import "./keyframes.css";

function AspectRatioTracker() {
  const [aspectRatio, setAspectRatio] = useState(null);

  useEffect(() => {
    function calculateAspectRatio() {
      return window.innerWidth / window.innerHeight;
    }

    function handleResize() {
      setAspectRatio(calculateAspectRatio());
    }

    if (typeof window !== "undefined") {
      setAspectRatio(calculateAspectRatio());
      window.addEventListener("resize", handleResize);

      return () => {
        window.removeEventListener("resize", handleResize);
      };
    }
  }, []);

  return aspectRatio;
}

const LOCAL_STORAGE_KEY = "productData";

export default function Home() {
  const [productData, setProductData] = useState([]);

  function isValidData(data) {
    return (
      data && data.data && data.data.price && Array.isArray(data.data.price)
    );
  }

  function saveDataToLocalStorage(data) {
    if (isValidData(data)) {
      localStorage.setItem(LOCAL_STORAGE_KEY, JSON.stringify(data));
    }
  }

  function getDataFromLocalStorage() {
    const storedData = localStorage.getItem(LOCAL_STORAGE_KEY);
    if (storedData) {
      return JSON.parse(storedData);
    }
    return null;
  }

  function handleError() {
    const storedData = getDataFromLocalStorage();
    console.error("Invalid data format in response");
    if (storedData) {
      setProductData(storedData.data.price);
      console.error("Used previous data from local storage");
    } else {
      console.error("There's no data in local storage");
    }
  }

  useEffect(() => {
    window.setData = function (data) {
      if (isValidData(data)) {
        console.log(data);
        setProductData(data.data.price);
        saveDataToLocalStorage(data);
      } else {
        handleError();
      }
    };
  }, []);

  function isValidData(data) {
    if (
      !data ||
      !data.data ||
      !data.data.price ||
      !Array.isArray(data.data.price)
    ) {
      return false;
    }

    return true;
  }

  const aspectRatio = AspectRatioTracker();

  let flexLayout;
  let divider;

  if (aspectRatio > 4) {
    // Extremely wide screens (e.g., 48:9 or wider)
    divider = 3;
  } else if (aspectRatio > 2) {
    // Wide screens (e.g., 32:9 or wider)
    divider = 2;
  } else if (aspectRatio > 1.5) {
    // Screens with aspect ratio between 16:9 and 32:9
    divider = 1;
  } else {
    // Screens with aspect ratio less than 16:9
    console.log(divider);
  }

  return (
    <>
      <Script id="manifest" type="application/json">
        {`
             {
              "name": "custom_insta_weather_price",
              "version": 1,
              "title": "Погода",
              "description": "",
              "options": [],
              "playback_data": {
                "price": {
                  "url": "'http://portal.visiobox.ru/docs/projects/meln/response_sample.json'",
                  "interval": 900
                }
              },
              "preview": {
                "width": 1920,
                "height": 1080
              }
            }
     `}
      </Script>
      <div className="header__div">
        {aspectRatio > 4 && (
          <>
            <Header />
          </>
        )}
        {aspectRatio > 3 && (
          <>
            <Header />
          </>
        )}
        {aspectRatio > 1.5 && (
          <>
            <Header />
          </>
        )}
      </div>
      <div className="background">
        {productData.length > 0 && (
          <Grid divider={divider} productData={productData}></Grid>
        )}
      </div>
      <div className="footer__div">
        {aspectRatio > 4 && (
          <>
            <Footer />
          </>
        )}
        {aspectRatio > 3 && (
          <>
            <Footer />
          </>
        )}
        {aspectRatio > 1.5 && (
          <>
            <Footer />
          </>
        )}
      </div>
      <Script
        id="application-debug"
        src="https://testing.visiobox.cloud/utils/application-debug.js"
        data-access-token="YGc7uf5Agi5LRc6yaE8dcQIuZh0HCX7g"
      ></Script>
    </>
  );
}
