/** @type {import('next').NextConfig} */
const nextConfig = {
  output: "export",
  productionSourceMaps: true,
  assetPrefix: "./",
  reactStrictMode: true,
  webpack: (config, { isServer }) => {
    // Disable minification for both client and server bundles
    if (!isServer) {
      config.optimization.minimize = false;
      config.optimization.minimizer = [];
    }

    return config;
  },
};

module.exports = nextConfig;
